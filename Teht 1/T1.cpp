#include <iostream>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <iterator>

int main() {

	std::ifstream f("sanoja.txt");

	std::ofstream t1;
	std::ofstream t2;
	std::ofstream t3;
	std::vector<std::string> pokemonVector;

	std::cout << "Opening output stream for tasks 1, 2 and 3..." << std::endl;
	t1.open("merkkijono.txt");
	t2.open("merkkijono_abc.txt");
	t3.open("merkkijono_abc_2.txt");

	std::cout << "Reading file sanoja.txt into vector..." << std::endl;
	for (std::string line; std::getline(f, line);) {
		pokemonVector.push_back(line);
	}
	std::cout << "Finished reading file" << std::endl;
	
	std::cout << "Iterating through vector, building files for tasks 1, 2 and 3..." << std::endl;
	std::sort(pokemonVector.begin(), pokemonVector.end());
	std::vector<std::string>::iterator it;
	for (it = pokemonVector.begin(); it < pokemonVector.end(); it++) {
		t1 << *it << std::endl;
		if ((*it).length() > 2) {
			t3 << *it << std::endl;
		}
		(*it).erase(std::remove_if((*it).begin(), (*it).end(), [](char c) {
			return !isalnum(c);
		}), (*it).end());
		t2 << *it << std::endl;
	}
	std::cout << "Done printing file contents, closing output streams..." << std::endl;
	t1.close();
	t2.close();
	t3.close();
	std::cout << "Output streams closed" << std::endl;
	system("pause");
	return 0;
}