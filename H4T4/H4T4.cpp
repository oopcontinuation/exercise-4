#include <iostream>
#include "Painter.h"

int main() {

	try {
		std::cout << "Creating five painters" << std::endl;
		Painter * p1 = Painter::luo();
		Painter * p2 = Painter::luo();
		Painter * p3 = Painter::luo();
		Painter * p4 = Painter::luo();
		Painter * p5 = Painter::luo();
		std::cout << "Five painters created, creating sixth one..." << std::endl;
		Painter * p6 = Painter::luo();
	}
	catch (const char *e) {
		std::cout << "Exception: " << e << std::endl;
	}
	catch (...) {
		std::cout << "Other exception" << std::endl;
	}


	system("pause");
	return 0;
}